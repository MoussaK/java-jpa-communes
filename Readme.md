# Reponse aux questions de cours sur JPA

## Exercice 3

*	Le fichier contient **39201** communes
*	Le champ **Ligne_5** correspond:
*	Le nombre de communes qui ont un **Libelle_acheminement** diff�rent du **Nom_commune** est �gale � **379**
*	On compte **383** communes dans le d�partement 72

## Exercice 4

*	Le fichier contient **35887** maires

```sql
SELECT COUNT(*) FROM jpa.maires;
```

*	Parmis ces 35887 maires ont a **5670** femmes et **30169** hommes

```sql
SELECT COUNT(*) FROM jpa.maires WHERE civilite='Mme';
SELECT COUNT(*) FROM jpa.maires WHERE civilite='M';
```

*	Le maire le plus ag�e a **97** ans.

```
SELECT * FROM jpa.maires 
			WHERE dateDeNaissance!='null'
		   		AND nom!=''
		   		ORDER BY dateDeNaissance LIMIT 1;
```

## Exercice 5

*	Il existe une relation **P->1** entre **Commune** et **Departement** (Plusieurs communes peuvent etre issues du m�me d�partement)

*	Oui on peut utiliser une �numeration pour le champs **civilite**;
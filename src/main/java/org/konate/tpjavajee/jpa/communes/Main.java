package org.konate.tpjavajee.jpa.communes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import org.konate.tpjavajee.jpa.communes.model.Departement;

public class Main {

	public static void main(String[] args) throws SQLException {
		
		String url = "jdbc:mysql://localhost:3306/jpa";
		String user = "jpa-user";
		String pwd = "jpa-password@";

		Connection connection = DriverManager.getConnection(url, user, pwd);
		Statement statement = connection.createStatement();
		
		
		//Communes
		ResultSet communesRS = statement.executeQuery("SELECT COUNT(*) FROM communes;");
		communesRS.next();
		System.out.println("Le fichier contient " + communesRS.getInt("COUNT(*)") + " communes");
		
		ResultSet rs = statement.executeQuery("SELECT COUNT(*) FROM communes WHERE libelle_acheminement!=nom_commune;");
		rs.next();
		System.out.println("Le nombre de communes qui ont un Libelle_acheminement différent du Nom_commune est " + rs.getInt("COUNT(*)"));
		
		ResultSet communeInRangeRS= statement.executeQuery("SELECT COUNT(*) FROM communes WHERE code_postal  REGEXP '72[0-9]{3}'");
		communeInRangeRS.next();
		System.out.println("On compte " + communeInRangeRS.getInt("COUNT(*)") + " communes dans le département 72");
		
		//Maires
		ResultSet mairesRS = statement.executeQuery("SELECT COUNT(*) FROM jpa.maires;");
		mairesRS.next();
		
		System.out.println("Le fichier contient : " + mairesRS.getInt("COUNT(*)") + " Maires");
		
		ResultSet maleMairesRS= statement.executeQuery("SELECT COUNT(*) FROM jpa.maires WHERE civilite='M';");
		maleMairesRS.next();
		
		int countMaires = maleMairesRS.getInt("COUNT(*)");
		ResultSet femaleMairesRS= statement.executeQuery("SELECT COUNT(*) FROM jpa.maires WHERE civilite='Mme';");
		femaleMairesRS.next();
		
		System.out.println("Parmis ces 35887 maires, on a " + countMaires + " Hommes et " + femaleMairesRS.getInt("COUNT(*)") + " Femmes");
		
		ResultSet maireInRangeRS = statement.executeQuery("SELECT COUNT(*) FROM jpa.maires WHERE dateDeNaissance >= '1974/01/01' AND dateDeNaissance <= '1996/01/01';");
		maireInRangeRS.next();
		
		System.out.println("le nombre de maires ayant l'age entre 18 et 30:  " + maireInRangeRS.getInt("COUNT(*)"));
		
		ResultSet dateDeNaissanceRS= statement.executeQuery("SELECT * FROM jpa.maires WHERE dateDeNaissance!='null' AND nom!='' ORDER BY dateDeNaissance LIMIT 1;");
		dateDeNaissanceRS.next();
		
		System.out.println("Le maire le plus age est de " + dateDeNaissanceRS.getDate("dateDeNaissance", Calendar.getInstance()));
		
		
		//Test 5 et 6
		System.out.println(Departement.getNumberOfTowns());
	}

}

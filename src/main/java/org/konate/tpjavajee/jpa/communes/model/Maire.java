package org.konate.tpjavajee.jpa.communes.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.konate.tpjavajee.jpa.communes.util.Civility;

@Entity(name="Maire")
@Table( name="maires")
public class Maire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(length=50)
	private String nom;
	
	@Column(length=50)
	private String prenom;
	
	@Column(length=5)
	@Enumerated(EnumType.STRING)
	private Civility civilite;
	
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	
	@OneToOne(mappedBy="maire") 
    private Commune commune ;

	public Maire() {
		super();
	}
	
	
}

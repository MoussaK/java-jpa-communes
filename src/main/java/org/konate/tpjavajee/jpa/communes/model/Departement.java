package org.konate.tpjavajee.jpa.communes.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Query;
import javax.persistence.Table;

import org.konate.tpjavajee.jpa.communes.util.Queries;

@Entity
@Table(name="departements")
public class Departement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(length=50)
	private String nom;
	
	@Column(length=50)
	private String code;
	
	@OneToMany(mappedBy="dpt")
	private Collection<Commune> communes;

	public Departement() {
		super();
	}
	
	public static int getNumberOfTowns() {
		
		EntityManager em = Queries.getEntityManager();
		Query query = em.createQuery("SELECT COUNT(commune) FROM Commune commune ") ;
		
		return  Integer.parseInt(query.getSingleResult().toString());
	}
	
	public static Maire getOldestMayor() {
		
		EntityManager em = Queries.getEntityManager();
		
		Query query = em.createQuery("SELECT maire FROM Maire maire ORDER BY maire.dateOfBirth DESC") ;
		return (Maire)query.getResultList().get(0);
	}
	
	public static double getWomanRatio() {
		
		EntityManager em = Queries.getEntityManager();
		Query query = em.createQuery("SELECT maire FROM Maire maire WHERE myMaire.civilite='Mme'");
		
		return 100*query.getResultList().size()/getNumberOfTowns();
	}
}

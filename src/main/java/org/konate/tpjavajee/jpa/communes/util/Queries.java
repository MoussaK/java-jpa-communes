package org.konate.tpjavajee.jpa.communes.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class Queries {
	private static EntityManager entityManager;
	
	public static EntityManager getEntityManager() {
		
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("jpa-test");
		entityManager = entityManagerFactory.createEntityManager();
		
		return entityManager;
	}
}

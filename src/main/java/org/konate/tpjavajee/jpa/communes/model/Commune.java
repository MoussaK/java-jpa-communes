package org.konate.tpjavajee.jpa.communes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name="Commune")
@Table( name="communes")
public class Commune implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Column(length=50)
	private String nom;
	
	@Column(length=50)
	private String code_postale;
	
	@ManyToOne
	private Departement dpt;
	
	@OneToOne
    private Maire maire;

	public Commune() {
		super();
	}

	public Commune(String nom, String code_postale) {
		super();
		this.nom = nom;
		this.code_postale = code_postale;
	}
}
